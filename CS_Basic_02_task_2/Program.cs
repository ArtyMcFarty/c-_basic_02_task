﻿using System;

namespace CS_Basic_02_task_2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            SwapArrayElements();
            LengthBetweenMaxElement();
        }

        public static void SwapArrayElements()
        {
            int[] Array1 = new int[] { 1, 2, 3, 4, 5, 6 };
            int[] Array2 = new int[Array1.Length];

            Array.ForEach(Array1, Console.WriteLine);
            Console.WriteLine("\n");

            for (int i = 0; i < Array1.Length; i++)
            {
                Array2[i] = Array1[Array1.Length - (i + 1)];
            }

            Array.ForEach(Array2, Console.WriteLine);
        }

        public static void LengthBetweenMaxElement()
        {
            int[] someArray = new int[] { 1, 54, 23, 5, 76, 45, 34 };

            int firstMax = 0;
            int lastMax = 0;
            int firstMaxIndex = 0;
            int lastMaxIndex = 0;
            int lengthBetweenMax = 0;

            for (int i = 1; i < someArray.Length; i++)
            {
                if ((someArray[i] > someArray[i - 1]) & (someArray[i] > firstMax))
                {
                    lastMax = firstMax;
                    lastMaxIndex = firstMaxIndex;
                    firstMax = someArray[i];
                    firstMaxIndex = i;
                }
            }

            lengthBetweenMax = firstMaxIndex - lastMaxIndex;

            Console.WriteLine("Length between max elements is {0}",lengthBetweenMax);

        }
    }
}
