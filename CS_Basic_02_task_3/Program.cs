﻿using System;

namespace CS_Basic_02_task_3
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int[,] matrix = new int[5, 5] { { 2, 4, 3, 3, 5 }, { 5, 7, 8, 5, 3 }, { 2, 4, 3, 3, 5 }, { 5, 7, 8, 5, 6 }, { 1, 5, 6, 3, 4 } };

            //display the initial matrix
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {
                    Console.Write(matrix[row, col] + " ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("\n\n\n");

            //display the final matrix
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {
                    //filling right side with 1
                    if (row != col & row < col)
                    {
                        matrix[row, col] = 1;
                    }
                    //filling left side with 0
                    else if (row != col & row > col)
                    {
                        matrix[row, col] = 0;
                    }

                    Console.Write(matrix[row, col] + " ");
                }
                Console.WriteLine();
            }

        }
    }
}
