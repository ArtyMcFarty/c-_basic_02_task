﻿using System;

namespace CS_Basic_02_task_1
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            ArithmeticAverage();
            TotalOfEvenNumbers();
            TotalOfEvenNumbersIf();
        }

        public static void ArithmeticAverage()
        {
            Console.WriteLine("Enter the first number ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter the last number ");
            int m = int.Parse(Console.ReadLine());

            int[] numbers = new int[(m + 1) - n];
            int index = 0;
            int total = 0;

            //Loop to fill numbers array
            for (int i = n; i <= m; i++)
            {
                numbers[index] = i;
                index = index + 1;
            }

            //Loop for getting total of numbers array elements
            for (int j = 0; j < numbers.Length; j++)
            {
                total = total + numbers[j];
            }

            int avarage = total / numbers.Length;


            Console.WriteLine("Arithmetic average in the sequence from {0} to {1} is {2} \n", n, m, avarage);
        }

        public static void TotalOfEvenNumbers()
        {
            Console.WriteLine("Enter the last number ");
            int n1 = int.Parse(Console.ReadLine());
            int evenTotal = 0;

            //Loop to get total of even-numbers
            for (int j = 0; j <= n1; j = j + 2)
            {
                evenTotal = evenTotal + j;
            }

            Console.WriteLine("Total of even numbers from 0 to {0} is {1} \n", n1, evenTotal);
        }

        public static void TotalOfEvenNumbersIf()
        {
            Console.WriteLine("Enter the first number ");
            int n1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the last number ");
            int n2 = int.Parse(Console.ReadLine());

            int evenTotal = 0;

            //Checking first number, if it's even - counting total from the first number
            if (n1%2 == 0)
            {
                for (int i = n1; i <= n2; i = i + 2)
                {
                    evenTotal = evenTotal + i;
                }
            }
            //if the first number is odd, add 1 and counting total
            else
            {
                for (int i = n1 + 1; i <= n2; i = i + 2)
                {
                    evenTotal = evenTotal + i;
                }
            }

            Console.WriteLine("Total of even numbers from {0} to {1} is {2} \n", n1, n2, evenTotal);
        }

    }   
}
